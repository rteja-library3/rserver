
# RServer

**Note:** description over here

## Installation

`go get gitlab.com/rteja-library3/rserver`

A dependency to the latest stable version should be automatically added to your `go.mod` file.

## Getting Started
Import the `rserver` package from Gitlab in your code:

```golang 
import "gitlab.com/rteja-library3/rserver"
```

## Examples

```golang 
package main

import (
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/rteja-library3/rserver"
)

type  logger  struct {
	logging *log.Logger
}

func (l logger) Info(args ...interface{}) {
	l.logging.Println("Info", args)
}

func  main() {
	hand := http.NewServeMux()
	hand.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Test"))
	})

	opts := rserver.DefaultOptions()
	opts.SetHandler(hand)

	logging := logger{
		log.New(os.Stdout, "[example]", 1),
	}

	rserver := rserver.NewServer(logging, opts)
	rserver.Start()

	csignal := make(chan os.Signal, 1)
	signal.Notify(csignal, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)

	// WAIT FOR IT
	<-csignal

	rserver.Close()
}
```

### Authors  

**Rahman Teja Wicaksono** - *rahman.9h.23@gmail.com*
- [Gitlab](https://gitlab.com/rteja-library3)
- [Linkedin](https://www.linkedin.com/in/rahman-teja-wicaksono-644518191/)