package rserver

import (
	"context"
	"log"
	"net/http"
	"os"
)

type Logger interface {
	Info(args ...interface{})
}

type Server struct {
	logger Logger
	server *http.Server
	port   string
	host   string
}

func NewServer(logger Logger, opts ...*Options) *Server {
	o := MustMergeServerOptions(opts...)

	server := &http.Server{
		Addr:         o.host + ":" + o.port,
		Handler:      o.handler,
		ReadTimeout:  o.readTimeout,
		WriteTimeout: o.writeTimeout,
		IdleTimeout:  o.idleTimeout,
		ErrorLog:     log.New(os.Stdout, "rserver", 1),
	}

	return &Server{
		logger: logger,
		server: server,
		port:   o.port,
		host:   o.host,
	}
}

func (s *Server) RunAndLog() {
	s.server.ListenAndServe()
}

func (s *Server) Start() {
	s.logger.Info("[rserver] HTTP Server on " + (s.host + ":" + s.port) + " starting")
	defer s.logger.Info("[rserver] HTTP Server on " + (s.host + ":" + s.port) + " started")

	go s.RunAndLog()
}

func (s *Server) Close() error {
	s.logger.Info("[rserver] HTTP Server on " + (s.host + ":" + s.port) + " shuting down")
	defer s.logger.Info("[rserver] HTTP Server on " + (s.host + ":" + s.port) + " gracefully shutdown")

	return s.server.Shutdown(context.Background())
}
