package rserver_test

import (
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rserver"
)

func TestNewServer(t *testing.T) {
	opt := rserver.DefaultOptions()

	srv := rserver.NewServer(logrus.New(), opt)

	assert.NotNil(t, srv, "[TestNewServer] Should not nil")
}

func TestNewServerStart(t *testing.T) {
	opt := rserver.DefaultOptions()

	srv := rserver.NewServer(logrus.New(), opt)
	srv.Start()

	assert.NotNil(t, srv, "[TestNewServerStart] Should not nil")
}

func TestNewServerClose(t *testing.T) {
	opt := rserver.DefaultOptions()

	srv := rserver.NewServer(logrus.New(), opt)
	srv.Close()

	assert.NotNil(t, srv, "[TestNewServerClose] Should not nil")
}
