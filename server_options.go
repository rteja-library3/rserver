package rserver

import (
	"net/http"
	"time"
)

type Options struct {
	handler      http.Handler
	host         string
	port         string
	readTimeout  time.Duration
	writeTimeout time.Duration
	idleTimeout  time.Duration
}

func NewOptions() *Options {
	return &Options{}
}

func DefaultOptions() *Options {
	return &Options{
		handler:      http.DefaultServeMux,
		host:         "localhost",
		port:         "9000",
		readTimeout:  30 * time.Second,
		writeTimeout: 30 * time.Second,
		idleTimeout:  30 * time.Second,
	}
}

func (o *Options) SetHandler(h http.Handler) *Options {
	o.handler = h
	return o
}

func (o *Options) SetHost(host string) *Options {
	o.host = host
	return o
}

func (o *Options) SetPort(port string) *Options {
	o.port = port
	return o
}

func (o *Options) SetReadTimeout(duration time.Duration) *Options {
	o.readTimeout = duration
	return o
}

func (o *Options) SetWriteTimeout(duration time.Duration) *Options {
	o.writeTimeout = duration
	return o
}

func (o *Options) SetIdleTimeout(duration time.Duration) *Options {
	o.idleTimeout = duration
	return o
}

func MergeServerOptions(opts ...*Options) (*Options, error) {
	o := NewOptions()

	for _, opt := range opts {
		if opt.handler != nil {
			o.handler = opt.handler
		}

		if opt.host != "" {
			o.host = opt.host
		}

		if opt.port != "" {
			o.port = opt.port
		}

		if opt.readTimeout > 0 {
			o.readTimeout = opt.readTimeout
		}

		if opt.writeTimeout > 0 {
			o.writeTimeout = opt.writeTimeout
		}

		if opt.idleTimeout > 0 {
			o.idleTimeout = opt.idleTimeout
		}
	}

	if o.port == "" {
		return nil, ErrPortIsRequired
	}

	if o.handler == nil {
		o.handler = http.DefaultServeMux
	}

	if o.host == "" {
		o.host = DefaultOptions().host
	}

	return o, nil
}

func MustMergeServerOptions(opts ...*Options) *Options {
	opt, err := MergeServerOptions(opts...)
	if err != nil {
		panic(err)
	}

	return opt
}
