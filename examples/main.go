package main

import (
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/rteja-library3/rserver"
)

type logger struct {
	logging *log.Logger
}

func (l logger) Info(args ...interface{}) {
	l.logging.Println("Info", args)
}

func main() {
	hand := http.NewServeMux()
	hand.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Test"))
	})

	opts := rserver.DefaultOptions()
	opts.SetHandler(hand)

	logging := logger{
		log.New(os.Stdout, "[example]", 1),
	}

	rserver := rserver.NewServer(logging, opts)
	rserver.Start()

	csignal := make(chan os.Signal, 1)
	signal.Notify(csignal, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	// WAIT FOR IT
	<-csignal

	rserver.Close()
}
