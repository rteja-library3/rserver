package rserver

import "fmt"

var (
	ErrPortIsRequired error = fmt.Errorf("err: Port is required")
)
