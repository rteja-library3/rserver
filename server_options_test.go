package rserver_test

import (
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rserver"
)

func TestNewOptions(t *testing.T) {
	opt := rserver.NewOptions()

	assert.NotNil(t, opt, "[TestNewOptions] Should not nil")
}

func TestDefaultOptions(t *testing.T) {
	opt := rserver.DefaultOptions()

	assert.NotNil(t, opt, "[TestDefaultOptions] Should not nil")
}

func TestNewOptionsSet(t *testing.T) {
	opt := rserver.NewOptions().
		SetHandler(http.DefaultServeMux).
		SetPort("9000").
		SetHost("localhost").
		SetReadTimeout(time.Second * 10).
		SetWriteTimeout(time.Second * 10).
		SetIdleTimeout(time.Second * 10)

	assert.NotNil(t, opt, "[TestNewOptionsSet] Should not nil")
}

func TestMergeServerOptions(t *testing.T) {
	opt := rserver.NewOptions().
		SetPort("9000").
		SetReadTimeout(time.Second * 10).
		SetWriteTimeout(time.Second * 10).
		SetIdleTimeout(time.Second * 10)

	opts, err := rserver.MergeServerOptions(opt)

	assert.NotNil(t, opts, "[TestNewOptionsSet] Should not nil")
	assert.NoError(t, err, "[TestNewOptionsSet] Error should nil")
}

func TestMergeServerOptionsEmpty(t *testing.T) {
	opt := rserver.NewOptions()

	opts, err := rserver.MergeServerOptions(opt)

	assert.Nil(t, opts, "[TestNewOptionsSet] Should nil")
	assert.Error(t, err, "[TestNewOptionsSet] Error should nil")
	assert.Equal(t, rserver.ErrPortIsRequired, err, "[TestNewOptionsSet] Error should ErrPortIsRequired")
}

func TestMustMergeServerOptionsEmpty(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()

	opt := rserver.NewOptions()

	opts := rserver.MustMergeServerOptions(opt)

	assert.Nil(t, opts, "[TestNewOptionsSet] Should nil")
}
