module gitlab.com/rteja-library3/rserver

go 1.17

require (
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.2.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
)
